# Create a Storybook Component Library

Storybook is a powerful UI develompent tool which allows you to build components 
and pages in isolation. The purpose of this task is to introduce you to Storybook
and the benefits of using it.

## Requirements

- All components should be organized into a single library.
- The component library should be configured with Storybook to run and 
  showcase all components. (See example below)
- Components that need data to be rendered should have it configured in
  their respective `stories` file.

### Example
![example](images/storybook-user_list-example.png)

#### Resources
- [Storybook Intro Video](https://www.youtube.com/watch?v=p-LFh5Y89eM&t=105s)
- [Storybook Documentation](https://storybook.js.org/docs/react/get-started/introduction)
- [NX Documentation for Storybook](https://nx.dev/l/a/storybook/overview)
